<?php

/**
 * Description of commands_model
 *
 * @author terminus
 */
class Commands_model extends CI_Model {

    private $tableName = 'commands';

    public function __construct() {
        parent::__construct();
    }

    public function getChild($parent_id) {
        $where['p_cmd_id'] = $parent_id;

        $result = $this->db->get_where($this->tableName, $where, 1);
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return FALSE;
    }

    public function getAll($only_count = 0, $limit = '', $offset = '') {
        if (1 != $only_count) {
            if ($limit > 0) {
                $this->db->limit($limit);
                if ($offset > 0) {
                    $this->db->limit($limit, $offset);
                }
            }
        }
        $result = $this->db->order_by('id', 'DESC')->get($this->tableName);
        if ($result->num_rows() > 0) {
            if (1 == $only_count) {
                return $result->num_rows();
            }
            return $result->result();
        }
        return FALSE;
    }

    public function get($command, $is_by_id = 1) {
        if (1 == $is_by_id) {
            $where['id'] = $command;
        } else {
            $where['command'] = $command;
        }

        $result = $this->db->get_where($this->tableName, $where, 1);
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return FALSE;
    }

    public function del($id) {
        $where['id'] = $id;

        $this->db->delete($this->tableName, $where, 1);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function add($command, $p_cmd_id = 0, $data_regex = '', $is_expire = 0, $is_with_plugin = 0, $plugin_name = '', $plugin_function = '', $reply_msgtype_id = 0, $content = '', $title = '', $description = '', $pic_url = '', $link_url = '', $music_url = '', $hqmusic_url = '') {
        if ($this->get($command, 0)) {
            return FALSE;
        }

        $set = array(
            'command' => $command,
            'p_cmd_id' => $p_cmd_id,
            'data_regex' => $data_regex,
            'is_expire' => $is_expire,
            'is_with_plugin' => $is_with_plugin,
            'plugin_name' => $plugin_name,
            'plugin_function' => $plugin_function,
            'reply_msgtype_id' => $reply_msgtype_id,
            'content' => $content,
            'title' => $title,
            'description' => $description,
            'pic_url' => $pic_url,
            'link_url' => $link_url,
            'music_url' => $music_url,
            'hqmusic_url' => $hqmusic_url
        );

        $this->db->insert($this->tableName, $set);
        $id = $this->db->insert_id();
        if ($id > 0) {
            return $id;
        }
        return FALSE;
    }

    public function edit($id, $command, $p_cmd_id = 0, $data_regex = '', $is_expire = 0, $is_with_plugin = 0, $plugin_name = '', $plugin_function = '', $reply_msgtype_id = 0, $content = '', $title = '', $description = '', $pic_url = '', $link_url = '', $music_url = '', $hqmusic_url = '') {
        $where['id'] = $id;
        $set = array(
            'command' => $command,
            'p_cmd_id' => $p_cmd_id,
            'data_regex' => $data_regex,
            'is_expire' => $is_expire,
            'is_with_plugin' => $is_with_plugin,
            'plugin_name' => $plugin_name,
            'plugin_function' => $plugin_function,
            'reply_msgtype_id' => $reply_msgtype_id,
            'content' => $content,
            'title' => $title,
            'description' => $description,
            'pic_url' => $pic_url,
            'link_url' => $link_url,
            'music_url' => $music_url,
            'hqmusic_url' => $hqmusic_url
        );

        if (TRUE === $this->db->update($this->tableName, $set, $where, 1)) {
            return TRUE;
        }
        return FALSE;
    }

}
