<?php

/**
 * Description of login
 *
 * @author terminus
 */
class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->language('ui/login');
    }

    public function index() {
        $this->load->view('login');
    }

    public function dologin() {
        $u = trim($this->input->post('username'));
        $p = trim($this->input->post('password'));

        $this->load->model('user_model', 'user');
        $user = $this->user->getUser($u);

        if (FALSE == $user) {
            message(lang('error'), lang('err_user_not_exist'), site_url('login'));
        }

        if ($user->password !== md5($p)) {
            message(lang('error'), lang('err_wrong_password'), site_url('login'));
        }

        $this->session->set_userdata(array(
            'uid' => $user->id,
            'uname' => $user->username,
            'ugroup' => $user->group
        ));

        $this->user->setUser($user->id, '', '', time(), 1);

        redirect('admin');
    }

}
