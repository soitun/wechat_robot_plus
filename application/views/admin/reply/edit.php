<script type="text/javascript">
    var acsource = [
<?php
$k = NULL;
foreach ($keywords as $v) {
    $k .= '{label:"' . $v->keyword . '",'
            . 'value:"' . $v->id . '"},';
}
echo rtrim($k, ',');
?>
    ];
    var msgtype_set = null;

    function show_msgtype_set(msgtype) {
        $("#msgtype_" + msgtype).fadeIn("fast").siblings().hide();
    }

    function get_keyword(kid) {
        var keyword = $("#keyword");
        var datas = {
            "do": "getkeyword",
            "id": kid
        };
        $.ajax({
            type: "POST",
            async: false,
            url: "<?php echo site_url('admin/ajax'); ?>",
            data: datas,
            dataType: "json",
            success: function(msg) {
//                console.debug(msg);
                if ('success' === msg['status']) {
                    keyword.html(msg['data']);
                } else {
                    keyword.html("<?php echo lang('err_keyword_not_exist'); ?>");
                }
            }
        });
    }

    $(document).ready(function() {
        //初始化
        msgtype_set = $("#select_msgtype").find("option:selected").text();
        show_msgtype_set(msgtype_set);
        get_keyword($("#keyword_id").val());
        //自动完成,获取keyword
        $("#keyword_id").autocomplete({source: acsource, delay: 100, minLength: 0})
                .blur(function() {
            var this_val = $(this).val();
            if (this_val.length > 0) {
                get_keyword(this_val);
            }
        });
        //标签切换
        $("#select_msgtype").change(function() {
            msgtype_set = $(this).find("option:selected").text();
            show_msgtype_set(msgtype_set);
        });
    });
</script>

<div id="win_r">
    <form action="<?php echo site_url('admin/reply_doedit'); ?>" method="post">
        <ul>
            <li>
                <input type="hidden" name="id" value="<?php echo $reply->id; ?>" />
                <span class="title"><?php echo lang('keyword'); ?>Id</span>
                <input type="text" id="keyword_id" name="keyword_id" value="<?php echo $reply->for_key_id; ?>" class="input" maxlength="100" />
                <span id="keyword" class="m_left_10"></span>
            </li>
            <li>
                <span class="title"><?php echo lang('reply_msgtype'); ?></span>
                <select id="select_msgtype" name="reply_msgtype_id" class="input">
                    <?php
                    foreach ($msgtype as $v) {
                        $selected = '';
                        if ($v->id == $reply->reply_msgtype_id) {
                            $selected = 'selected';
                        }
                        $tmp = '<option '
                                . $selected
                                . ' value="' . $v->id . '">'
                                . $v->type_name
                                . '</option>';
                        echo $tmp;
                    }
                    ?>
                </select>
                <span class="m_left_10 notice"><?php echo lang('reply_msgtype'); ?></span>
            </li>
            <li>
                <div id="msgtype_text" class="hide">
                    <span class="title"><?php echo lang('reply_content'); ?></span>
                    <textarea name="content" class="input_area"><?php echo $reply->content; ?></textarea>
                </div>
                <div id="msgtype_news" class="hide">
                    <ul>
                        <li>
                            <span class="title"><?php echo lang('title'); ?></span>
                            <input type="text" name="news_title" value="<?php echo $reply->title; ?>" class="wide_input" />
                        </li>
                        <li>
                            <span class="title"><?php echo lang('description'); ?></span>
                            <textarea name="news_description" class="input_area"><?php echo $reply->description; ?></textarea>
                        </li>
                        <li>
                            <span class="title"><?php
                                echo empty($reply->pic_url) ? lang('pic_url') : ('<a target="_blank" href="'
                                        . $reply->pic_url . '">'
                                        . lang('pic_url') . '</a>');
                                ?></span>
                            <input type="text" id="pic_url" name="pic_url" value="<?php echo $reply->pic_url; ?>" class="wide_input" />
                            <div id="pic_uploader" style="display:inline-block;vertical-align:middle"> 
                                <div style="display:inline-block;width:30px;height:30px;overflow:hidden;background:url(<?php echo public_res('images/folder.png'); ?>)">
                                    <input style="opacity:0" name="userfile" type="file" />
                                </div>
                                <input style="margin-left:10px" type="button" value="<?php echo lang('upload'); ?>" onclick="iframe_uploader('pic_uploader', '<?php echo site_url('admin/doupload'); ?>', 'pic_url');" />
                            </div>
                        </li>
                        <li>
                            <span class="title"><?php
                                echo empty($reply->link_url) ? lang('link_url') : ('<a target="_blank" href="'
                                        . $reply->link_url . '">'
                                        . lang('link_url') . '</a>');
                                ?></span>
                            <input type="text" name="link_url" value="<?php echo $reply->link_url; ?>" class="wide_input" />
                        </li>
                    </ul>
                </div>
                <div id="msgtype_music" class="hide">
                    <ul>
                        <li>
                            <span class="title"><?php echo lang('title'); ?></span>
                            <input type="text" name="music_title" value="<?php echo $reply->title; ?>" class="wide_input" />
                        </li>
                        <li>
                            <span class="title"><?php echo lang('description'); ?></span>
                            <textarea name="music_description" class="input_area"><?php echo $reply->description; ?></textarea>
                        </li>
                        <li>
                            <span class="title"><?php
                                echo empty($reply->music_url) ? lang('music_url') : ('<a target="_blank" href="'
                                        . $reply->music_url . '">'
                                        . lang('music_url') . '</a>');
                                ?></span>
                            <input type="text" id="music_url" name="music_url" value="<?php echo $reply->music_url; ?>" class="wide_input" />
                            <div id="music_uploader" style="display:inline-block;vertical-align:middle"> 
                                <div style="display:inline-block;width:30px;height:30px;overflow:hidden;background:url(<?php echo public_res('images/folder.png'); ?>)">
                                    <input style="opacity:0" name="userfile" type="file" />
                                </div>
                                <input style="margin-left:10px" type="button" value="<?php echo lang('upload'); ?>" onclick="iframe_uploader('music_uploader', '<?php echo site_url('admin/doupload'); ?>', 'music_url');" />
                            </div>
                        </li>
                        <li>
                            <span class="title"><?php
                                echo empty($reply->hqmusic_url) ? lang('hqmusic_url') : ('<a target="_blank" href="'
                                        . $reply->hqmusic_url . '">'
                                        . lang('hqmusic_url') . '</a>');
                                ?></span>
                            <input type="text" id="hqmusic_url" name="hqmusic_url" value="<?php echo $reply->hqmusic_url; ?>" class="wide_input" />
                            <div id="hqmusic_uploader" style="display:inline-block;vertical-align:middle"> 
                                <div style="display:inline-block;width:30px;height:30px;overflow:hidden;background:url(<?php echo public_res('images/folder.png'); ?>)">
                                    <input style="opacity:0" name="userfile" type="file" />
                                </div>
                                <input style="margin-left:10px" type="button" value="<?php echo lang('upload'); ?>" onclick="iframe_uploader('hqmusic_uploader', '<?php echo site_url('admin/doupload'); ?>', 'hqmusic_url');" />
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="text_c">
                <input type="submit" value="<?php echo lang('edit'); ?>" onclick="submit();" />
            </li> 
        </ul>
    </form>
</div>